from django.shortcuts import render




def Story1(request):
    return render(request, 'main/Story1.html')

def Story3_aboutme(request):
    return render(request, 'main/Story3_aboutme.html')

def Story3_contact(request):
    return render(request, 'main/Story3_contact.html')

def Story3_eduskill(request):
    return render(request, 'main/Story3_eduskill.html')

def home(request):
    return render(request, 'main/home.html')