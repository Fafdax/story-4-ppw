from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.Story1, name='Story1'),
    path('Story3_aboutme', views.Story3_aboutme, name='aboutme'),
    path('Story3_contact', views.Story3_contact, name='contact'),
    path('Story3_eduskill', views.Story3_eduskill, name='eduskill'),
    path('home', views.home, name='home'),
]
