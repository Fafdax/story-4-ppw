from django.urls import path
from . import views

app_name = 'webs8'

urlpatterns = [
    path('', views.home, name="s8h"),
    path('data/', views.data, name="s8gd"),
]