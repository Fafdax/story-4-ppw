from django.test import TestCase, Client
from django.urls import reverse
import urllib3
import json

# Create your tests here.
class story8_test(TestCase):
    def test_url(self):
        response = self.client.get(reverse('webs8:s8h'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "story8_home.html")

    def test_data(self):
       response = self.client.get(reverse('webs8:s8gd') + '?q=a')
       self.assertEqual(response.status_code, 200)

