$( function() {
    $( "#accordion" ).accordion({
        collapsible: true,
        header: "> div > h3"
    });
    $(".naik, .turun").click(function(event){
        event.stopPropagation();
        const tombol = $(this);
        const blok = tombol.parent().parent();
        if(tombol.is(".naik")) blok.insertBefore(blok.prev())
        else blok.insertAfter(blok.next())
    });
} );