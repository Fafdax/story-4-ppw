from django.urls import path
from . import views

app_name = 'webs7'

urlpatterns = [
    path('', views.story7_home, name="s7h"),
]