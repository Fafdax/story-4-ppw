from . import views
from django.urls import path

app_name = 'webs9'

urlpatterns = [
    path('', views.story9, name ='s9'),
    path('signup', views.signup, name='s9s'),
    path('login', views.login, name='s9li'),
    path('logout', views.logout, name='s9lo'),
]